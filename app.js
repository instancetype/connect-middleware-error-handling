/**
 * Created by instancetype on 6/9/14.
 */
var connect = require('connect');

var api = connect()
    .use(users)
    .use(pets)
    .use(errorHandler);

var app = connect()
    .use(hello)
    .use('/api', api)
    .use(errorPage)
    .listen(3000);


function hello(req, res, next) {
    if (req.url.match(/^\/hello/)) {
        res.end('Hello World\n');

    } else {
        next();
    }
}

var db = {
    users: [
        { name: 'kalymba'},
        { name: 'jitsu'},
        { name: 'sydney'}
    ]
};

function users(req, res, next) {
    var match = req.url.match(/^\/user\/(.+)/);
    if (match) {
        var username = [match[1]].toString();
        var found = false;
        db.users.forEach(function(user) {

            if (user.name === username) {
                found = true;
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify(username));
            }
        });
        if (!found) {
            var err = new Error('User not found');
            err.notFound = true;
            next(err);
        }
    } else {
        next();
    }
}

function pets(req, res, next) {
    if (req.url.match(/^\/pet\/(.+)/)) {
        foo(); // will trigger an exception
    } else {
        next();
    }
}

function errorHandler(err, req, res, next) {
    console.error(err.stack);
    res.setHeader('Content-Type', 'application/json');
    if (err.notFound) {
        res.statusCode = 404;
        res.end(JSON.stringify({ error: err.message }));
    } else {
        res.statusCode = 500;
        res.end(JSON.stringify({ error: 'Internal Server Error' }));
    }
}

function errorPage(err, req, res, next) {
    console.error(err.stack);
    res.setHeader('Content-Type', 'text/plain');
    res.statusCode = 400;
    res.end('Something went wonky!');
}
